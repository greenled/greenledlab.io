---
title: About
---

:wave: Hi! I'm Juan Carlos Mejías Rodríguez (AKA **greenLED**), a Cuban software engineer. I build software usually within small, agile teams, mostly for the web.

I dream of creating products which make people happier, and one day I will be part of a massive transformation that will put a smile in no less than half of the world population. I know, it's a huge goal. And it's beautiful too! Isn't it? Meanwhile, I try to learn as much as I can and share my experiences.

If you want to know what I'm into you should check my [LinkedIn](https://www.linkedin.com/in/jcmejiasrodriguez), [Twitter](https://twitter.com/greenled2013), [GitHub](https://github.com/greenled) and [GitLab](https://gitlab.com/greenled) profiles. Spoiler: prepare to see Open Source and DevOps-related content, as well as nerdy jokes.
